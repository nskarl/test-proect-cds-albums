<?php

namespace application\controllers;

use application\core\Controller;
use application\models\Artists;

class MainController extends Controller
{
	public function actionIndex()
	{
		$artists = new Artists();

		$data = [
			'list' => $this->model->getAll(),
			'artists' => $artists->getAll(),
		];
		
		$this->view->render('Список альбомов', $data);
	}
}