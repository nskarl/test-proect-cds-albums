<?php

namespace application\controllers;

use application\core\Controller;
use application\models\Main;
use application\models\Artists;

class AdminController extends Controller
{
	public function actionIndex()
	{
		$this->view->layout = 'default';

		$list = new Main();

		$data = [
			'list' => $list->getAll(),
		];
		
		$this->view->render('Страница администрирования', $data);
	}

	public function actionAdd()
	{
		$artists = new Artists();

		$data = [
			'artists' => $artists->getAll(),
		];

		$this->view->render('Страница добавления альбома', $data);
	}

	public function actionEdit()
	{
		$gets = $_GET;

		if(!isset($gets['id']))
			$this->view->redirect('/404');

		if(!$id = (int) $gets['id'])
			$this->view->redirect('/404');

		$artists = new Artists();
		$cd = new Main();

		$data = [
			'cd' => $cd->getOne($id),
			'artists' => $artists->getAll(),
		];
		
		$this->view->render('Страница редактирования альбома', $data);
	}
}