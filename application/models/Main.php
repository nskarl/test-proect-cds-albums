<?php

namespace application\models;

class Main extends \application\core\Model
{
	public function getAll()
	{
		$gets = $_GET;

		$artist = isset($gets['artist']) ? (int)$gets['artist'] : false;
		$sort = isset($gets['sort']) ? $gets['sort'] : false;

		switch ($sort) {
			case 'year_desc':
				$order = 'cds.date_release';
				$by = 'DESC';
				break;

			case 'year_asc':
				$order = 'cds.date_release';
				$by = 'ASC';
				break;

			default:
				$order = 'cds.id';
				$by = 'ASC';
				break;
		}

		return $this->db->all('
			SELECT cds.*, artists.name as artist_name, racks.number as rack_number, rooms.number as room_number, shelfs.number as shelf_number
			FROM cds
			LEFT JOIN artists ON artists.id = cds.artist_id
			LEFT JOIN racks ON racks.id = cds.rack_id
			LEFT JOIN rooms ON rooms.id = cds.room_id
			LEFT JOIN shelfs ON shelfs.id = cds.shelf_id
			' . ($artist ? 'WHERE cds.artist_id = ' . $artist : '') . '
			ORDER BY ' . $order . ' ' . $by);
	}

	public function getOne(int $id)
	{
		if(!$id)
			return [];

		return $this->db->one('SELECT * FROM cds WHERE cds.id = ' . $id);
	}
}