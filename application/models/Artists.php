<?php

namespace application\models;

class Artists extends \application\core\Model
{
	public function getAll()
	{
		return $this->db->all('SELECT * FROM artists ORDER BY name ASC');
	}
}