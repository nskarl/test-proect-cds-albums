<?php
	
$gets = $_GET;
$artist = isset($gets['artist']) ? (int)$gets['artist'] : '';
$sort = isset($gets['sort']) ? $gets['sort'] : '';

?>
<div>
	<form action="/" method="get" class="m-b" onChange="this.submit();">
	
		<div class="row m-b">
			<div class="col-sm-3">
				<label>Артисты</label><br />
				<select name="artist" class="form-control">
					<option value="">Все артисты</option>
					<?php foreach($artists as $one): ?>
					<option <?php if($artist == $one['id']): ?>selected="selected"<?php endif; ?> value="<?= $one['id']; ?>"><?= $one['name']; ?></option>
					<?php endforeach;?>
				</select>
			</div>
			<div class="col-sm-3">
				<label>Сортировка</label><br />
				<select name="sort" class="form-control">
					<option value="">Без сортировки</option>
					<option <?php if($sort == 'year_desc'): ?>selected="selected"<?php endif; ?> value="year_desc">Год: убывание</option>
					<option <?php if($sort == 'year_asc'): ?>selected="selected"<?php endif; ?> value="year_asc">Год: возрастание</option>
				</select>
			</div>
		</div>
		<div class="row m-b">
			
		</div>
	</form>
</div>

<table width="100%">
	<thead>
		<tr>
			<th>ID</th>
			<th>Обложка</th>
			<th>Название альбома</th>
			<th>Артист</th>
			<th>Год выпуска</th>
			<th>Длительность, мин.</th>
			<th>Дата покупки</th>
			<th>Стоимость покупки</th>
			<th>Код хранилища</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($list as $row): ?>
		<tr>
			<td><?= $row['id']; ?></td>
			<td><?= $row['cover']; ?></td>
			<td><?= $row['name']; ?></td>
			<td><?= $row['artist_name']; ?></td>
			<td><?= $row['date_release']; ?></td>
			<td><?= $row['duration']; ?></td>
			<td><?= $row['date_buy']; ?></td>
			<td><?= $row['price_buy']; ?></td>
			<td><?= $row['room_number']; ?>:<?= $row['rack_number']; ?>:<?= $row['shelf_number']; ?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>