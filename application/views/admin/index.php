<a href="/admin/add" class="btn btn-danger btn-xs m-b-lg">Добавить</a>
<table width="100%">
	<thead>
		<tr>
			<th>ID</th>
			<th>img</th>
			<th>Название альбома</th>
			<th>Артист</th>
			<th>Год выпуска</th>
			<th>Длительность, мин.</th>
			<th>Дата покупки</th>
			<th>Стоимость покупки</th>
			<th>Код хранилища</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($list as $row): ?>
		<tr>
			<td><?= $row['id']; ?></td>
			<td><?= $row['cover']; ?></td>
			<td><?= $row['name']; ?></td>
			<td><?= $row['artist_name']; ?></td>
			<td><?= $row['date_release']; ?></td>
			<td><?= $row['duration']; ?></td>
			<td><?= $row['date_buy']; ?></td>
			<td><?= $row['price_buy']; ?></td>
			<td><?= $row['room_number']; ?>:<?= $row['rack_number']; ?>:<?= $row['shelf_number']; ?></td>
			<td><a href="/admin/edit?id=<?= $row['id']; ?>" class="btn btn-danger btn-xs">Ред.</a></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>