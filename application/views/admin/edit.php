<form method="post" class="m-b">

	<div class="m-b">
		<label>Название</label><br />
		<input type="text" name="name" class="form-control" value="<?= $cd['name']; ?>" />
		<hr />
	</div>

	<div class="m-b">
		<label>Обложка</label><br />
		<input type="file" name="cover" class="form-control" />
		<hr />
	</div>

	<div class="m-b">
		<label>Артист</label><br />
		<select name="artist" class="form-control">
			<?php foreach($artists as $one): ?>
			<option <?php if($cd['artist_id'] == $one['id']): ?>selected="selected"<?php endif; ?> value="<?= $one['id']; ?>"><?= $one['name']; ?></option>
			<?php endforeach;?>
		</select>
		<hr />
	</div>

</form>