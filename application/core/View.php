<?php

namespace application\core;

class View 
{
	public $path;
	public $route;
	public $layout = 'default';

	public function __construct($route)
	{
		$this->route = $route;
		$this->path = $route['controller'] . '/' . $route['action'];
	}

	public function render(string $title, $data = [])
	{
		extract($data);
		if(file_exists('application/views/' . $this->path . '.php')) {
			ob_start();
			require 'application/views/' . $this->path . '.php';
			$content = ob_get_clean();
			require 'application/views/layouts/' . $this->layout . '.php';
		} else {
			throw new \Exception('View file not found.');
		}
	}

	public function redirect($url)
	{
		header('location: ' . $url);
		exit;
	}

	public static function error($code)
	{
		http_response_code($code);

		$path = 'application/views/errors/' . $code . '.php';
		
		if(file_exists($path))
			require $path;
		else
			throw new \Exception('Error file not found.');
		exit;
	}
}