<?php

return [

	'' => [
		'controller' => 'main',
		'action' => 'index',
	],

	'main/index' => [
		'controller' => 'main',
		'action' => 'index',
	],

	'admin' => [
		'controller' => 'admin',
		'action' => 'index',
	],

	'admin/index' => [
		'controller' => 'admin',
		'action' => 'index',
	],

	'admin/add' => [
		'controller' => 'admin',
		'action' => 'add',
	],

	'admin/edit' => [
		'controller' => 'admin',
		'action' => 'edit',
	],
];