<?php

namespace application\lib;

use PDO;

class Db
{
	protected $db;

	public function __construct()
	{
		$config = require 'application/config/db.php';
		
		try {
			$this->db = new PDO('mysql:host=' . $config['host'] . ';port=' . $config['port'] . ';dbname=' . $config['dbname'], $config['user'], $config['password'], array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
		} catch (\Exception $e) {
		    echo $e->getMessage(), "\n";
		}
	}

	public function query($sql)
	{
		$query = $this->db->query($sql);
		return $query;
	}

	public function one($sql)
	{
		$result = $this->query($sql);
		return $result->fetch(PDO::FETCH_ASSOC);
	}

	public function all($sql)
	{
		$result = $this->query($sql);
		return $result->fetchAll(PDO::FETCH_ASSOC);
	}
}