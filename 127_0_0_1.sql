SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `test`;

CREATE TABLE `artists` (
  `id` int(11) NOT NULL,
  `name` varchar(32) CHARACTER SET utf8 NOT NULL,
  `birthday` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `artists` (`id`, `name`, `birthday`) VALUES
(1, 'Pink Floyd', '1972-02-19 11:34:41'),
(2, 'AC/DC', '1972-02-19 11:34:41'),
(3, 'Майкл Джексон', '2019-02-19 11:35:19'),
(4, 'Eagles', '2019-02-19 11:35:19'),
(5, 'Мадонна', '2019-02-19 11:35:31'),
(6, 'Metallica', '2019-02-19 11:35:31'),
(7, 'Nirvana', '2019-02-19 11:35:59'),
(8, 'Queen', '2019-02-19 11:35:59');

CREATE TABLE `cds` (
  `id` int(11) NOT NULL,
  `cover` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `artist_id` int(11) NOT NULL,
  `date_release` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `date_buy` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `price_buy` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `rack_id` int(11) NOT NULL,
  `shelf_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `cds` (`id`, `cover`, `name`, `artist_id`, `date_release`, `duration`, `date_buy`, `price_buy`, `room_id`, `rack_id`, `shelf_id`) VALUES
(1, NULL, 'The Dark Side of the Moon', 1, 1973, 53, '2019-02-19 12:35:01', 2000, 1, 2, 3),
(2, NULL, 'Back in Black', 2, 1980, 44, '2019-02-19 12:38:49', 185, 2, 3, 4),
(3, NULL, 'Dangerous', 3, 1991, 45, '2019-02-19 12:41:57', 225, 1, 3, 4),
(4, NULL, 'Bad', 3, 1987, 66, '2019-02-19 12:42:22', 425, 2, 4, 4),
(5, NULL, 'HIStory: Past, Present and Future, Book I', 3, 1995, 63, '2019-02-19 12:42:56', 235, 4, 3, 1),
(6, NULL, 'Their Greatest Hits (1971–1975)', 4, 1976, 45, '2019-02-19 12:43:24', 123, 4, 4, 4),
(7, NULL, 'The Immaculate Collection', 5, 1990, 56, '2019-02-19 12:44:02', 124, 2, 2, 3),
(8, NULL, 'True Blue', 5, 1986, 68, '2019-02-19 12:44:32', 445, 3, 3, 1),
(9, NULL, 'Like a Virgin', 5, 1984, 78, '2019-02-19 12:45:06', 365, 2, 1, 4),
(10, NULL, 'Ray of Light', 5, 1998, 81, '2019-02-19 12:45:33', 442, 1, 1, 1),
(11, NULL, 'Metallica', 6, 1991, 56, '2019-02-19 12:46:02', 445, 2, 4, 2),
(12, '', 'Nevermind', 7, 1991, 47, '2019-02-19 14:16:48', 446, 3, 1, 1),
(13, NULL, 'Greatest Hits', 8, 1981, 69, '2019-02-19 12:47:00', 255, 1, 4, 4);

CREATE TABLE `racks` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `racks` (`id`, `number`, `name`) VALUES
(1, 1, 'Стойка 1'),
(2, 2, 'Стойка 2'),
(3, 3, 'Стойка 3'),
(4, 4, 'Стойка 4');

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `name` varchar(32) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `rooms` (`id`, `number`, `name`) VALUES
(1, 1, 'Комната 1'),
(2, 2, 'Комната 2'),
(3, 3, 'Комната 3'),
(4, 4, 'Комната 4');

CREATE TABLE `shelfs` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `shelfs` (`id`, `number`, `name`) VALUES
(1, 1, 'Полка 1'),
(2, 2, 'Полка 2'),
(3, 3, 'Полка 3'),
(4, 4, 'Полка 4');


ALTER TABLE `artists`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `cds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shelf_id` (`shelf_id`),
  ADD KEY `room_id` (`room_id`),
  ADD KEY `artist_id` (`artist_id`),
  ADD KEY `date_release` (`date_release`);

ALTER TABLE `racks`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `shelfs`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `artists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

ALTER TABLE `cds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

ALTER TABLE `racks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `shelfs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
